#include <opencv4/opencv2/imgproc.hpp>
#include <opencv4/opencv2/highgui/highgui.hpp>
#include <iostream>
#include <vector>

#include "lineDetection.hpp"

int lineDetection(std::string fileName)
{

    cv::VideoCapture dashCam(fileName);
    if (!dashCam.isOpened())
    {
        std::cout << "Error opening video file" << std::endl;
        return -1;
    }

    while (1)
    {
        float scale = 0.60;

        cv::Mat oryginalFrame, frame;
        dashCam >> frame;
        dashCam >> oryginalFrame;

        if (frame.empty())
        {
            break;
        }

        int sensitivity = 100;
        cv::Scalar lowerWhiteThreshold = cv::Scalar(0, 0, 255 - sensitivity); //lines on the road are more grayish than pure white, hence sensitivity
        cv::Scalar upperWhiteThreshold = cv::Scalar(255, sensitivity, 255);

        cv::imshow("oryginal", oryginalFrame);

        cv::Mat frameWhiteExtracted = frame;
        frame(cv::Rect(0, frame.rows / 2, frame.cols, frame.rows / 2 - (frame.rows * 0.05))); //crop image to contain only ...
        //a bottom half without camera signs

        for (int x = 0; x < frameWhiteExtracted.cols; ++x) // blackout top half of the picture + bottom stripe of camera info (CASE specific)
        {
            for (int y = 0; y < frameWhiteExtracted.rows; ++y)
            {
                if (y < (frameWhiteExtracted.rows / 2) || y > (frameWhiteExtracted.rows * 0.95))
                {
                    cv::Vec3b &color = frameWhiteExtracted.at<cv::Vec3b>(y, x);
                    color[0] = 0;
                    color[1] = 0;
                    color[2] = 0;
                }
            }
        }

        cv::cvtColor(frame, frame, cv::COLOR_RGB2HSV); //convertion to HSV for white color extraction

        inRange(frameWhiteExtracted, lowerWhiteThreshold, upperWhiteThreshold, frameWhiteExtracted); //extract white/gray road lines
        cv::Canny(frameWhiteExtracted, frameWhiteExtracted, 50, 200, 3);                             // Detect lines using Canny detector
        //cv::imshow("Lines in white onlyy ", frameWhiteExtracted);

        std::vector<cv::Vec4i> lines;                                        //vector to store polar coordinate system parameters of detected lines
        HoughLinesP(frameWhiteExtracted, lines, 1, CV_PI / 180, 50, 50, 10); //(dst,lines,rhoResolution,thetaResolution,threshold,minLineLen,maxLineGap)

        for (size_t i = 0; i < lines.size(); i++) //Plot lines on oryginalFrame
        {
            cv::Vec4i l = lines[i];
            line(oryginalFrame, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(255, 255, 0), 3); //(frame.rows / 2 - (frame.rows * 0.05))), cv::Point(l[2], l[3] + (frame.rows / 2 - (frame.rows * 0.05))), cv::Scalar(255, 255, 0), 3);
        }

        cv::imshow("Lines highlighted ", oryginalFrame);
        cv::resize(frame, frame, cv::Size(), scale, scale);

        char c = (char)cv::waitKey(25);
        if (c == 27)
        {
            break;
        }
    }
    dashCam.release();
    cv::destroyAllWindows();
    return 0;
}

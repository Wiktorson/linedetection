set(lineDetectionSourceFiles
    lineDetection.cpp
    lineDetection.hpp
)
add_library(lineDetection STATIC ${lineDetectionSourceFiles} )

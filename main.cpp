//Line Detection Project 15.03.2020
#include <iostream>
#include "src/lineDetection.hpp"

int main(int argc, char **argv)
{
	std::string fileName{"dashcam_example.mp4"};
	return lineDetection(fileName);
}

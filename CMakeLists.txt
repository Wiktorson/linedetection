#OpenCV needs to be installed on the system

cmake_minimum_required(VERSION 3.10.2)

project("Line Detection and Autonomous Driving")

message("Processing cmake files for Line Detection / Autonomous Driving project.")

add_definitions(
-Wfatal-errors
)

find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})
add_subdirectory(src)

add_executable(LineDetectionApp main.cpp)
set_target_properties(LineDetectionApp PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})

target_link_libraries(LineDetectionApp lineDetection ${OpenCV_LIBS})
